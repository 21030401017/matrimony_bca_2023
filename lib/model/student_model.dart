class StudentModel{
  int? _StudentId;

  int? get StudentId => _StudentId;

  set StudentId(int? value) {
    _StudentId = value;
  }
  String? _Name;

  String? get Name => _Name;

  set Name(String? value) {
    _Name = value;
  }
  String? _EnrollmentNo;

  String? get EnrollmentNo => _EnrollmentNo;

  set EnrollmentNo(String? value) {
    _EnrollmentNo = value;
  }

  StudentModel.name(this._StudentId, this._Name, this._EnrollmentNo);
}