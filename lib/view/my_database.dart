import 'dart:io';

import 'package:matrimony/model/student_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MyDatabase {
  Future<Database> getDatabaseReference() async {
    Directory dir = await getApplicationDocumentsDirectory();
    return await openDatabase(
      join(dir.path, 'LMS.db'),
      version: 1,
      onCreate: (db, version) {
        db.execute(
            'CREATE TABLE Tbl_Student(Student INTEGER PRIMARY KEY AUTOINCREMENT, Name Text, EnrollmentNo Text)');
      },
    );
  }

  Future<List<StudentModel>> getStudentList() async {
    Database db = await getDatabaseReference();
    List<Map<String, Object?>> studentList =
        await db.rawQuery('SELECT * FROM Tbl_Student');
    return List.generate(
      studentList.length,
      (index) => StudentModel.name(
        studentList[index]['Student'] as int,
        studentList[index]['Name'].toString(),
        studentList[index]['EnrollmentNo'].toString(),
      ),
    );
  }

  Future<int> insertStudentInTblStudent(
      {required String name, required String enrollmentNo}) async {
    Map<String, dynamic> map = {};
    map['Name'] = name;
    map['EnrollmentNo'] = enrollmentNo;

    Database db = await getDatabaseReference();
    return await db.insert('Tbl_Student', map);
  }
}
