import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony/view/my_database.dart';

class AddStudentPage extends StatelessWidget {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();
  TextEditingController enrollmentController = TextEditingController();

  MyDatabase db = MyDatabase();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: Text(
          'Add Student',
          style: TextStyle(
            color: Colors.white,
            fontSize: 16,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(children: [
            TextFormField(
              controller: nameController,
              decoration: InputDecoration(
                hintText: 'Enter Name',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    15,
                  ),
                ),
              ),
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter Valid Name';
                }
                return null;
              },
            ),
            SizedBox(
              height: 10,
            ),
            TextFormField(
              controller: enrollmentController,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Enter Enrollment No.';
                }
                if (value.isNotEmpty && value.length != 10) {
                  return 'Enter Valid Enrollment No.';
                }
                return null;
              },
              decoration: InputDecoration(
                hintText: 'Enter Enrollment No.',
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    15,
                  ),
                ),
              ),
            ),
            TextButton(
              onPressed: () async {
                if (_formKey.currentState!.validate()) {
                  int primaryKey = await db.insertStudentInTblStudent(
                    name: nameController.value.text,
                    enrollmentNo: enrollmentController.value.text,
                  );
                  if (primaryKey <= 0) {
                    showCupertinoDialog(
                      context: context,
                      builder: (context) {
                        return CupertinoAlertDialog(
                          title: Text('Error in Operation'),
                          actions: [
                            TextButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text('Ok'))
                          ],
                        );
                      },
                    );
                  } else {
                    showCupertinoDialog(
                      context: context,
                      builder: (context) {
                        return CupertinoAlertDialog(
                          title: Text('Data Inserted Successfully'),
                          actions: [
                            TextButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                                Navigator.of(context).pop(true);
                              },
                              child: Text('Ok'),
                            )
                          ],
                        );
                      },
                    );
                  }
                }
              },
              child: Text(
                'Save',
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(
                  Colors.pink,
                ),
              ),
            )
          ]),
        ),
      ),
    );
  }
}
