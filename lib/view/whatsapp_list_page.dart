import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony/view/add_whatsapp_content.dart';
import 'package:matrimony/view/login_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class WhatsappListPage extends StatefulWidget {
  @override
  State<WhatsappListPage> createState() => _WhatsappListPageState();
}

class _WhatsappListPageState extends State<WhatsappListPage> {
  List<Map<String, dynamic>> userList = [];

  void insertUser({
    required String userName,
    required String userImage,
    required String message,
    required int counter,
  }) {
    Map<String, dynamic> map = {};
    map['UserName'] = userName;
    map['Image'] = userImage;
    map['LastMessage'] = message;
    map['Counter'] = counter;
    userList.add(map);
  }

  @override
  void initState() {
    super.initState();
    insertUser(
        userName: 'Mehul',
        userImage: 'assets/images/bg_matrimony_prelogin.jpg',
        message: 'Last Message from raj',
        counter: 5);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          'Whatsapp',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          InkWell(
            child: Icon(Icons.logout),
            onTap: () {
              showCupertinoDialog(
                context: context,
                builder: (context) {
                  return CupertinoAlertDialog(
                    content: Text('Are you sure want to logout?'),
                    actions: [
                      TextButton(
                        onPressed: () async {
                          SharedPreferences preferences =
                              await SharedPreferences.getInstance();
                          preferences.setBool('IsLogin', false);
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(
                              builder: (context) {
                                return LoginPage();
                              },
                            ),
                          );
                        },
                        child: Text(
                          'Yes',
                        ),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('No'),
                      ),
                    ],
                  );
                },
              );
            },
          )
        ],
      ),
      body: userList.isNotEmpty
          ? ListView.builder(
              itemCount: userList.length,
              itemBuilder: (context, index) {
                Map<String, dynamic> map = userList[index];
                return InkWell(
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) {
                          return AddWhatsappPage(
                            name: map['UserName'],
                            image: map['Image'],
                            messsage: map['LastMessage'],
                            counter: map['Counter'],
                            isEdit: true,
                          );
                        },
                      ),
                    ).then((value) {
                      setState(() {
                        userList[index] = value;
                      });
                    });
                  },
                  child: Card(
                    elevation: 5,
                    shadowColor: Colors.red,
                    margin: const EdgeInsets.all(10),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          CircleAvatar(
                            foregroundImage: AssetImage(map['Image']),
                            backgroundColor: Colors.red,
                            foregroundColor: Colors.black,
                            radius: 25,
                          ),
                          Expanded(
                            child: Container(
                              margin: const EdgeInsets.only(left: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    map['UserName'],
                                    style: const TextStyle(
                                        color: Colors.black, fontSize: 20),
                                  ),
                                  Text(
                                    map['LastMessage'],
                                    style: const TextStyle(
                                        color: Colors.grey, fontSize: 16),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          map['Counter'] != null &&
                                  (int.parse(map['Counter'].toString())) > 0
                              ? Container(
                                  decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.green,
                                  ),
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                    map['Counter'].toString(),
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                    ),
                                  ),
                                )
                              : Container(),
                          InkWell(
                            child: const Icon(
                              Icons.delete,
                              color: Colors.red,
                              size: 24,
                            ),
                            onTap: () {
                              setState(() {
                                userList.removeAt(index);
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            )
          : const Center(child: Text('No User Found')),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return AddWhatsappPage(
                  isEdit: false,
                );
              },
            ),
          ).then(
            (value) {
              setState(
                () {
                  Map<String, dynamic> map = value;
                  userList.add(map);
                },
              );
            },
          );
        },
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
      ),
    );
  }
}
