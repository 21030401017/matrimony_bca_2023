import 'package:flutter/material.dart';

class AddWhatsappPage extends StatelessWidget {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _messageController = TextEditingController();
  TextEditingController _imageController = TextEditingController();
  TextEditingController _counterController = TextEditingController();

  bool isEdit = false;

  AddWhatsappPage({String? name,String? messsage,String? image,String? counter,bool? isEdit}){
    _nameController.text = name??'';
    _messageController.text = messsage??'';
    _imageController.text = image??'';
    _counterController.text = (counter??'').toString();
    this.isEdit = isEdit??false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text(
          '${isEdit?'Edit':'Add'} User',
        ),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            getCustomTextFormField(
              hintText: 'Enter Name',
              textEditingController: _nameController,
            ),
            getCustomTextFormField(
              hintText: 'Enter Message',
              textEditingController: _messageController,
            ),
            getCustomTextFormField(
              hintText: 'Enter Image Url',
              textEditingController: _imageController,
            ),
            getCustomTextFormField(
              hintText: 'Enter Counter',
              textEditingController: _counterController,
            ),
            SizedBox(
              height: 10,
            ),
            TextButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  Colors.green,
                ),
              ),
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  Map<String, dynamic> map = {};
                  map['UserName'] = _nameController.text.toString();
                  map['Image'] = _imageController.text.toString();
                  map['LastMessage'] = _messageController.text.toString();
                  map['Counter'] = _counterController.text.toString();
                  Navigator.of(context).pop(map);
                }
              },
              child: Text(
                'Submit',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget getCustomTextFormField(
      {String? hintText, TextEditingController? textEditingController}) {
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
        controller: textEditingController,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return hintText;
          }
        },
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          hintText: hintText ?? 'Enter Value',
        ),
      ),
    );
  }
}
